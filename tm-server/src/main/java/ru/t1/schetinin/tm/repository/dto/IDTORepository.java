package ru.t1.schetinin.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.schetinin.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public interface IDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}