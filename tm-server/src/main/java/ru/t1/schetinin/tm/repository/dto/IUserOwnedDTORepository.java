package ru.t1.schetinin.tm.repository.dto;

import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.schetinin.tm.dto.model.AbstractUserOwnedModelDTO;

@NoRepositoryBean
public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

}
