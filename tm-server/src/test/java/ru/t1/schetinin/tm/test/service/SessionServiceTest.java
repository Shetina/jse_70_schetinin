package ru.t1.schetinin.tm.test.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.api.service.dto.ISessionDTOService;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.api.service.dto.IUserDTOService;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.dto.model.SessionDTO;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.service.dto.ProjectDTOService;
import ru.t1.schetinin.tm.service.dto.SessionDTOService;
import ru.t1.schetinin.tm.service.dto.TaskDTOService;
import ru.t1.schetinin.tm.service.dto.UserDTOService;
import ru.t1.schetinin.tm.test.migration.AbstractSchemeTest;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    public final static SessionDTO SESSION_TEST1 = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION_TEST2 = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION_TEST3 = new SessionDTO();

    @NotNull
    public final static String SESSION_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private final ISessionDTOService SESSION_SERVICE = new SessionDTOService(CONNECTION_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
        SESSION_TEST1.setRole(user.getRole());
        SESSION_TEST2.setRole(user.getRole());
        SESSION_TEST3.setRole(user.getRole());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin("test_login");
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Before
    public void initDemoData() throws Exception {
        SESSION_SERVICE.add(USER_ID, SESSION_TEST1);
        SESSION_SERVICE.add(USER_ID, SESSION_TEST2);
    }

    @After
    public void clearData() throws Exception {
        SESSION_SERVICE.clear(USER_ID);
    }

    @Test
    public void testAddSession() throws Exception {
        Assert.assertNotNull(SESSION_SERVICE.add(USER_ID, SESSION_TEST3));
        @Nullable final SessionDTO session = SESSION_SERVICE.findOneById(USER_ID, SESSION_TEST3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_TEST3.getId(), session.getId());
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final SessionDTO session = SESSION_SERVICE.findOneById(USER_ID, SESSION_TEST1.getId());
        Assert.assertTrue(SESSION_SERVICE.existsById(USER_ID, session.getId()));
        Assert.assertTrue(SESSION_SERVICE.existsById(session.getUserId(), session.getId()));
        Assert.assertFalse(SESSION_SERVICE.existsById(USER_ID, SESSION_ID_FAKE));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<SessionDTO> sessions = SESSION_SERVICE.findAll(USER_ID);
        Assert.assertEquals(sessions.size(), SESSION_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<SessionDTO> sessions = SESSION_SERVICE.findAll(USER_ID);
        @NotNull final SessionDTO sessions1 = sessions.get(0);
        @NotNull final String sessionsId = sessions1.getId();
        Assert.assertEquals(sessions1.getId(), SESSION_SERVICE.findOneById(USER_ID, sessionsId).getId());
    }

    @Test
    public void testClearUser() throws Exception {
        SESSION_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SESSION_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testRemoveById() throws Exception {
        SESSION_SERVICE.removeById(USER_ID, SESSION_TEST2.getId());
        Assert.assertNull(SESSION_SERVICE.findOneById(USER_ID, SESSION_TEST2.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        SESSION_SERVICE.remove(USER_ID, SESSION_TEST2);
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(USER_ID, null));
    }

}