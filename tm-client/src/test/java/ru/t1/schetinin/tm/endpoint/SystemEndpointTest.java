package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.request.ServerAboutRequest;
import ru.t1.schetinin.tm.dto.request.ServerVersionRequest;
import ru.t1.schetinin.tm.dto.response.ServerAboutResponse;
import ru.t1.schetinin.tm.marker.SoapCategory;
import ru.t1.schetinin.tm.service.PropertyService;

@Category(SoapCategory.class)
public class SystemEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final ISystemEndpoint SYSTEM_ENDPOINT = ISystemEndpoint.newInstance(PROPERTY_SERVICE);

    @Test
    public void testGetVersion() {
        @NotNull final ServerVersionRequest serverVersionRequest = new ServerVersionRequest();
        Assert.assertNotNull(SYSTEM_ENDPOINT.getVersion(serverVersionRequest).getVersion());
    }

    @Test
    public void testGetAbout() {
        @NotNull final ServerAboutRequest serverAboutRequest = new ServerAboutRequest();
        Assert.assertNotNull(SYSTEM_ENDPOINT.getAbout(serverAboutRequest).getEmail());
        Assert.assertNotNull(SYSTEM_ENDPOINT.getAbout(serverAboutRequest).getName());
    }

}