package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.schetinin.tm.api.endpoint.ProjectEndpoint;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.schetinin.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("isAuthenticated()")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    @PreAuthorize("isAuthenticated()")
    public ProjectDTO add(@WebParam(name = "project", partName = "project") @RequestBody final @NotNull ProjectDTO project) throws Exception {
        return projectService.addByUserId(UserUtil.getUserId(), project);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    public ProjectDTO save(@WebParam(name = "project", partName = "project") @RequestBody final @NotNull ProjectDTO project) throws Exception {
        return projectService.updateByUserId(UserUtil.getUserId(), project);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public ProjectDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") final @NotNull String id) throws Exception {
        return projectService.findOneByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("isAuthenticated()")
    public boolean existsById(@WebParam(name = "id", partName = "id") @PathVariable("id") final @NotNull String id) throws Exception {
        return projectService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public long count() throws Exception {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") final @NotNull String id) throws Exception {
        projectService.removeByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("isAuthenticated()")
    public void delete(@WebParam(name = "project", partName = "project") @RequestBody final @NotNull ProjectDTO project) throws Exception {
        projectService.removeByUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    @PreAuthorize("isAuthenticated()")
    public void clear() throws Exception {
        projectService.clearByUserId(UserUtil.getUserId());
    }

}