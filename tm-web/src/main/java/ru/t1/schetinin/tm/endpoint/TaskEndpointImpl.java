package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.schetinin.tm.api.endpoint.TaskEndpoint;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.model.dto.TaskDTO;
import ru.t1.schetinin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.schetinin.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("isAuthenticated()")
    public List<TaskDTO> findAll() throws Exception {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/findAllByProjectId/{projectId}")
    public List<TaskDTO> findAllByProjectId(@WebParam(name = "projectId", partName = "projectId") @PathVariable("projectId") final @NotNull String projectId) throws Exception {
        return taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    @PreAuthorize("isAuthenticated()")
    public TaskDTO add(@WebParam(name = "task", partName = "task") @RequestBody final @NotNull TaskDTO task) throws Exception {
        return taskService.addByUserId(UserUtil.getUserId(), task);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    public TaskDTO save(@WebParam(name = "task", partName = "task") @RequestBody final @NotNull TaskDTO task) throws Exception {
        return taskService.updateByUserId(UserUtil.getUserId(), task);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public TaskDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") final @NotNull String id) throws Exception {
        return taskService.findOneByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("isAuthenticated()")
    public boolean existsById(@WebParam(name = "id", partName = "id") @PathVariable("id") final @NotNull String id) throws Exception {
        return taskService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public long count() throws Exception {
        return taskService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") final @NotNull String id) throws Exception {
        taskService.removeByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("isAuthenticated()")
    public void delete(@WebParam(name = "task", partName = "task") @RequestBody final @NotNull TaskDTO task) throws Exception {
        taskService.removeByUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    @PreAuthorize("isAuthenticated()")
    public void clear() throws Exception {
        taskService.clearByUserId(UserUtil.getUserId());
    }

}