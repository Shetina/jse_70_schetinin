package ru.t1.schetinin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.api.service.model.IProjectService;
import ru.t1.schetinin.tm.api.service.model.ITaskService;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.model.dto.CustomUser;
import ru.t1.schetinin.tm.model.dto.TaskDTO;

@Controller
public class TaskController {

    @Autowired
    private ITaskDTOService taskDTOService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    @PreAuthorize("isAuthenticated()")
    public String create(@AuthenticationPrincipal final CustomUser user) throws Exception {
        taskDTOService.addByUserId(user.getUserId(), new TaskDTO("New Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(@AuthenticationPrincipal final CustomUser user, @PathVariable("id") String id) throws Exception {
        taskService.removeByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(@AuthenticationPrincipal final CustomUser user, @ModelAttribute("task") Task task, BindingResult result) throws Exception {
        if (task.getProject() == null)
            task.setProject(null);
        else task.setProject(projectService.findOneByUserIdAndId(user.getUserId(), task.getProject().getName()));
        taskService.updateByUserId(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(@AuthenticationPrincipal final CustomUser user, @PathVariable("id") String id) throws Exception {
        @NotNull final Task task = taskService.findOneByUserIdAndId(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

}