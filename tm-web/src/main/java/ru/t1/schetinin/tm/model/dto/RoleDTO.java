package ru.t1.schetinin.tm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "role_table")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RoleDTO {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }
}