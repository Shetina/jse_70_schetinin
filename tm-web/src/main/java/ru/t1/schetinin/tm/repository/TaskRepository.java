package ru.t1.schetinin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

    long countByUser(@NotNull final User user);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @NotNull
    List<Task> findByUser(@NotNull final User user);

    @NotNull
    List<Task> findByUser(@NotNull final User user, @NotNull final Sort sort);

    @NotNull
    Optional<Task> findByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteByUser(@NotNull final User user);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findByUserAndProjectId(@NotNull final User user, @NotNull final String projectId);

}