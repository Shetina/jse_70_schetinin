package ru.t1.schetinin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.User;

import java.util.List;

public interface IUserService {

    @NotNull
    User add(@NotNull User model) throws Exception;

    void clear() throws Exception;

    int count() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<User> findAll() throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable User model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @Nullable
    User update(@Nullable User model) throws Exception;

    @Nullable
    User updateById(@Nullable String id, @Nullable String login, @Nullable String passwordHash) throws Exception;

}
